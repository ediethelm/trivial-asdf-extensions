;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-asdf-extensions)

(mgl-pax:defsection @trivial-asdf-extensions-manual (:title "Trivial ASDF Extensions Manual")
  "[![pipeline status](https://gitlab.com/ediethelm/trivial-asdf-extensions/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-asdf-extensions/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-asdf-extensions.svg)](http://quickdocs.org/trivial-asdf-extensions/)
[![coverage report](https://gitlab.com/ediethelm/trivial-asdf-extensions/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/trivial-asdf-extensions/-/jobs/artifacts/master/browse?job=test-coverage)"
  (@trivial-asdf-extensions-description mgl-pax:section)
  (@trivial-asdf-extensions-installing mgl-pax:section)
  (@trivial-asdf-extensions-example mgl-pax:section)
  (@trivial-asdf-extensions-exported mgl-pax:section)
  (@trivial-asdf-extensions-license mgl-pax:section)
  (@trivial-asdf-extensions-contributing mgl-pax:section))


(mgl-pax:defsection @trivial-asdf-extensions-description (:title "Description")
  "")

(mgl-pax:defsection @trivial-asdf-extensions-installing (:title "Installing trivial-asdf-extensions")
    "")

(mgl-pax:defsection @trivial-asdf-extensions-example (:title "Working Example")
  "")

(mgl-pax:defsection @trivial-asdf-extensions-exported (:title "Exported Symbols")
  )

(mgl-pax:defsection @trivial-asdf-extensions-license (:title "License Information")
  "This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-asdf-extensions/blob/master/LICENSE 'License') to get the full licensing text.")

(mgl-pax:defsection @trivial-asdf-extensions-contributing (:title "Contributing to this project")
  "Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-asdf-extensions/blob/master/CONTRIBUTING.md 'Contributing') document for more information.")
