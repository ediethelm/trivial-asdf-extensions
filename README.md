# Trivial ASDF Extensions Manual

###### \[in package TRIVIAL-ASDF-EXTENSIONS\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-asdf-extensions/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-asdf-extensions/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-asdf-extensions.svg)](http://quickdocs.org/trivial-asdf-extensions/)
[![coverage report](https://gitlab.com/ediethelm/trivial-asdf-extensions/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/trivial-asdf-extensions/-/jobs/artifacts/master/browse?job=test-coverage)

## Description



## Installing trivial-asdf-extensions



## Working Example



## Exported Symbols


## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-asdf-extensions/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-asdf-extensions/blob/master/CONTRIBUTING.md "Contributing") document for more information.
