;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-asdf-extensions
  :name "trivial-asdf-extensions"
  :description ""
  :version "0.0.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :components ((:file "package")
	       (:file "trivial-asdf-extensions")))

(defsystem :trivial-asdf-extensions/document
  :name "trivial-asdf-extensions/document"
  :description ""
  :version "0.0.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-asdf-extensions
	       :mgl-pax)
  :components ((:file "documentation")))
