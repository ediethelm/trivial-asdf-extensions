(uiop:define-package #:trivial-asdf-extensions
  (:documentation "")
  (:use #:common-lisp
	:uiop/common-lisp :uiop :asdf/upgrade
	:asdf/component :asdf/operation
	:asdf/system :asdf/find-system :asdf/defsystem
	:asdf/action :asdf/lisp-action :asdf/bundle)
  (:export #:install-op
	   #:document-op
	   #:run-op))

