(in-package :trivial-asdf-extensions)

(defclass install-op (asdf:non-propagating-operation)
  ())

(defclass document-op (selfward-operation)
  ((selfward-operation :initform '(asdf:load-op))))

(defclass run-op (selfward-operation)
  ((selfward-operation :initform '(asdf:load-op))))

